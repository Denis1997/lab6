//
//  Analyzer.h
//  Task1
//
//  Created by Admin on 13.04.16.
//  Copyright © 2016 Denis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Analyzer : NSObject

-(void)analyze:(NSString *) bar;

@end
