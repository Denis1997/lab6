///
//  Analyzer.m
//  Task1
//
//  Created by Admin on 13.04.16.
//  Copyright © 2016 Denis. All rights reserved.
//

#import "Analyzer.h"

@implementation Analyzer

-(void)analyze:(NSString *)bar
{
    NSArray *words = [[bar substringToIndex:[bar length] - 1] componentsSeparatedByString:@" "];
    
    NSMutableDictionary *statistics = [[NSMutableDictionary alloc] init];
    
    NSNumber *repetitions = 0;
    
    for (NSString *word in words)
    {
        repetitions = [statistics valueForKey:word];
        [statistics setObject:[[NSNumber alloc] initWithLong:([repetitions integerValue] + 1)]forKey: word];
    }
    
    NSNumber *maximum = [[NSNumber alloc] initWithLong: 0];
    NSMutableArray *firstFive = [[NSMutableArray alloc] init];
    
    for (id key in statistics)
    {
        if ([statistics[key] compare: maximum] == NSOrderedDescending)
        {
            [firstFive removeAllObjects];
            maximum = [[NSNumber alloc] initWithLong:[statistics[key] integerValue]];
            [firstFive addObject: key];
        }
        else
            if ([statistics[key] compare: maximum] == NSOrderedSame)
            {
                [firstFive addObject: key];
            }
    }
    
    int i = 0;
    for (NSString *word in firstFive)
    {
        NSLog(@"%@: %@", word, maximum);
        i++;
        if (i == 5)
            return;
    }
}

@end
