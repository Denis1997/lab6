//
//  main.m
//  Task1
//
//  Created by Admin on 13.04.16.
//  Copyright © 2016 Denis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Analyzer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        char buffer[255];
        fgets(buffer, 255, stdin);
        unsigned long lineLength = strlen(buffer);
        
        Analyzer *analyzer = [[Analyzer alloc] init];
        
        NSString *text = [[NSString alloc] initWithBytes:buffer
                                            length:lineLength
                                            encoding: NSASCIIStringEncoding];
        
        [analyzer analyze:text];
    }
    return 0;
}
