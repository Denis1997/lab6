//
//  ViewController.m
//  task2
//
//  Created by Admin on 13.04.16.
//  Copyright © 2016 Denis. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *indicator;
@property (weak, nonatomic) IBOutlet UITextField *location;

@end

@implementation ViewController
- (IBAction)refresh:(id)sender {
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat: @"%1$@%2$@%3$@", @"https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.temp%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22", [self location].text, @"%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"]];
    NSData *contents = [[NSData alloc] initWithContentsOfURL:url];
    NSDictionary *forcasting = [NSJSONSerialization JSONObjectWithData:contents options:NSJSONReadingMutableContainers error:nil];
    NSString *temp = [NSString stringWithFormat:@"%1$@",forcasting[@"query"][@"results"][@"channel"][@"item"][@"condition"][@"temp"]];
    double dtemp = [temp doubleValue];
    dtemp = (dtemp - 32) / 9 * 5.0;
    [[self indicator] setText: [NSString stringWithFormat: @"%.2lf С", dtemp]];
    [self indicator].backgroundColor = [UIColor colorWithRed:(((int)(dtemp * 10000)) % 255)/255.0f
                    green:((int)(dtemp * 10000))/255.0f
                     blue:255.0f/255.0f
                    alpha:1.0f];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
